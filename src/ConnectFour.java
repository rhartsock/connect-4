import java.util.Scanner;
import java.util.Stack;

public class ConnectFour {
	
	////MAIN////
	public static void main(String args[])
	{
		Stack<Board> stack = new Stack<Board>();
		
		Board board = new Board();
		int turn = 2;
		int depth = 0;
		int minimax;
		//board.boardState[6][4] = 1;
		
		for(int i = 0; i < 10; i ++)
		{
			//depth += 1;
			turn = 2;
			generateChildren(board, turn, stack);
			minimax = -1000000;
			for(int j = 0; j < stack.size(); j++)
			{
				if(stack.get(j).minimaxValue > minimax)
				{
					minimax = stack.get(j).minimaxValue;
					board = stack.get(j);
				}
			}
			stack.clear();
			printBoard(board);
			if(checkGameState(board, turn) == turn)
			{
				System.out.println("WIN for 2");
				break;
			}	
			else
				System.out.println("NOT SURE YET");
			System.out.println("MIN:" + board.minimaxValue);
			System.out.println("+++++++++");

			/*turn = 1;
			generateChildren(board, turn, stack);
			minimax = -1;
			for(int j = 0; j < stack.size(); j++)
			{
				if(stack.get(j).minimaxValue > minimax)
				{
					minimax = stack.get(j).minimaxValue;
					board = stack.get(j);
				}
			}
			stack.clear();*/
			turn = 1;
			playerMove(board, turn);
			printBoard(board);
			if(checkGameState(board, turn) == turn)
			{
				System.out.println("WIN for 1");
				break;
			}
				
			else
				System.out.println("NOT SURE YET");
			System.out.println("MIN:" + board.minimaxValue);
			System.out.println("+++++++++");
		}
	}
	////END MAIN////
	
	////GENERATE CHILDREN////
	static void generateChildren(Board board, int turn, Stack<Board> stack)
	{
		for(int i = 0; i < 8; i++)
		{
			for(int j = 6; j >= 0; j--)
			{
				if(board.boardState[j][i] == 0)
				{
					Board child = new Board();
					for(int k = 0; k < 7; k++)
					{
						for(int l = 0; l < 8; l++)
						{
							child.boardState[k][l] = board.boardState[k][l];
						}
					}
					child.parent = board;
					child.boardState[j][i] = turn;
					calculateMinimax(child, turn);
					stack.push(child);
					break;
				}
				
			}
		}
	}
	////END GENERATE CHILDREN////
	
	////DETERMINE MINIMAX VALUE////
	static void calculateMinimax(Board board, int turn)
	{
		for(int i = 0; i < 8; i++)
		{
			for(int j = 6; j >= 0; j--)
			{
				if(board.boardState[j][i] == turn)
				{
					//i-1
					//Two in a row
					if(i-1 >= 0 && board.boardState[j][i-1] == turn)
					{
							board.minimaxValue += 1;
							
							//Three in a row
							if(i-2 >= 0 && board.boardState[j][i-2] == turn)
							{
									board.minimaxValue += 10;
									
									//Four on a row
									if(i-3 >= 0 && board.boardState[j][i-3] == turn)
									{
											board.minimaxValue += 100;
									}
							}
					}
					
					//i+1
					//Two in a row
					if(i+1 <= 7 && board.boardState[j][i+1] == turn)
					{
							board.minimaxValue += 1;
							
							//Three in a row
							if(i+2 <= 7 && board.boardState[j][i+2] == turn)
							{
									board.minimaxValue += 10;
									
									//Four on a row
									if(i+3 <= 7 && board.boardState[j][i+3] == turn)
									{
											board.minimaxValue += 100;
									}
							}
					}
					
					//j-1
					//Two in a row
					if(j-1 >= 0 && board.boardState[j-1][i] == turn)
					{
							board.minimaxValue += 1;
							
							//Three in a row
							if(j-2 >= 0 && board.boardState[j-2][i] == turn)
							{
									board.minimaxValue += 10;
									
									//Four on a row
									if(j-3 >= 0 && board.boardState[j-3][i] == turn)
									{
											board.minimaxValue += 100;
									}
							}
					}
					
					//j+1
					//Two in a row
					if(j+1 <= 6 && board.boardState[j+1][i] == turn)
					{					
							board.minimaxValue += 1;
							
							//Three in a row
							if(j+2 <= 6 && board.boardState[j+2][i] == turn)
							{					
									board.minimaxValue += 10;
									
									//Four on a row
									if(j+3 <= 6 && board.boardState[j+3][i] == turn)
									{					
											board.minimaxValue += 100;
									}
							}
					}
					
					//i+1 & j+1
					//Two in a row
					if(i+1 <= 7 && j+1 <= 6 && board.boardState[j+1][i+1] == turn)
					{
							board.minimaxValue += 1;
							
							//Three in a row
							if(i+2 <= 7 && j+2 <= 6 && board.boardState[j+2][i+2] == turn)
							{
									board.minimaxValue += 10;
									
									//Four on a row
									if(i+3 <= 7 && j+3 <= 6 && board.boardState[j+3][i+3] == turn)
									{
											board.minimaxValue += 100;
									}
							}
					}
					
					//i+1 & j-1
					//Two in a row
					if(i+1 <= 7 && j-1 >= 0 && board.boardState[j-1][i+1] == turn)
					{
							board.minimaxValue += 1;
							
							//Three in a row
							if(i+2 <= 7 && j-2 >= 0 && board.boardState[j-2][i+2] == turn)
							{
									board.minimaxValue += 10;
									
									//Four on a row
									if(i+3 <= 7 && j-3 >= 0 && board.boardState[j-3][i+3] == turn)
									{
											board.minimaxValue += 100;
									}
							}
					}
					
					//i-1 & j+1
					//Two in a row
					if(i-1 >= 0 && j+1 <= 6 && board.boardState[j+1][i-1] == turn)
					{
							board.minimaxValue += 1;
							
							//Three in a row
							if(i-2 >= 0 && j+2 <= 6 && board.boardState[j+2][i-2] == turn)
							{
									board.minimaxValue += 10;
									
									//Four on a row
									if(i-3 >= 0 && j+3 <= 6 && board.boardState[j+3][i-3] == turn)
									{
											board.minimaxValue += 100;
									}
							}
					}
					
					//i-1 & j-1
					//Two in a row
					if(i-1 >= 0 && j-1 >= 0 && board.boardState[j-1][i-1] == turn)
					{
							board.minimaxValue += 1;
							
							//Three in a row
							if(i-2 >= 0 && j-2 >= 0 && board.boardState[j-2][i-2] == turn)
							{
									board.minimaxValue += 10;
									
									//Four on a row
									if(i-3 >= 0 && j-3 >= 0 && board.boardState[j-3][i-3] == turn)
									{
											board.minimaxValue += 100;
									}
							}
					}	
				
				}
			}
		}
		
		if(turn == 1)
			turn = 2;
		else if(turn == 2)
			turn = 1;
		
		for(int i = 0; i < 8; i++)
		{
			for(int j = 6; j >= 0; j--)
			{
				if(board.boardState[j][i] == turn)
				{
					//i-1
					//Two in a row
					if(i-1 >= 0 && board.boardState[j][i-1] == turn)
					{
							board.minimaxValue -= 1;
							
							//Three in a row
							if(i-2 >= 0 && board.boardState[j][i-2] == turn)
							{
									board.minimaxValue -= 50;
									
									//Four on a row
									if(i-3 >= 0 && board.boardState[j][i-3] == turn)
									{
											board.minimaxValue -= 100;
									}
							}
					}
					
					//i+1
					//Two in a row
					if(i+1 <= 7 && board.boardState[j][i+1] == turn)
					{
							board.minimaxValue -= 1;
							
							//Three in a row
							if(i+2 <= 7 && board.boardState[j][i+2] == turn)
							{
									board.minimaxValue -= 50;
									
									//Four on a row
									if(i+3 <= 7 && board.boardState[j][i+3] == turn)
									{
											board.minimaxValue -= 100;
									}
							}
					}
					
					//j-1
					//Two in a row
					if(j-1 >= 0 && board.boardState[j-1][i] == turn)
					{
							board.minimaxValue -= 1;
							
							//Three in a row
							if(j-2 >= 0 && board.boardState[j-2][i] == turn)
							{
									board.minimaxValue -= 50;
									
									//Four on a row
									if(j-3 >= 0 && board.boardState[j-3][i] == turn)
									{
											board.minimaxValue -= 100;
									}
							}
					}
					
					//j+1
					//Two in a row
					if(j+1 <= 6 && board.boardState[j+1][i] == turn)
					{					
							board.minimaxValue -= 1;
							
							//Three in a row
							if(j+2 <= 6 && board.boardState[j+2][i] == turn)
							{					
									board.minimaxValue -= 50;
									
									//Four on a row
									if(j+3 <= 6 && board.boardState[j+3][i] == turn)
									{					
											board.minimaxValue -= 100;
									}
							}
					}
					
					//i+1 & j+1
					//Two in a row
					if(i+1 <= 7 && j+1 <= 6 && board.boardState[j+1][i+1] == turn)
					{
							board.minimaxValue -= 1;
							
							//Three in a row
							if(i+2 <= 7 && j+2 <= 6 && board.boardState[j+2][i+2] == turn)
							{
									board.minimaxValue -= 50;
									
									//Four on a row
									if(i+3 <= 7 && j+3 <= 6 && board.boardState[j+3][i+3] == turn)
									{
											board.minimaxValue -= 100;
									}
							}
					}
					
					//i+1 & j-1
					//Two in a row
					if(i+1 <= 7 && j-1 >= 0 && board.boardState[j-1][i+1] == turn)
					{
							board.minimaxValue -= 1;
							
							//Three in a row
							if(i+2 <= 7 && j-2 >= 0 && board.boardState[j-2][i+2] == turn)
							{
									board.minimaxValue -= 50;
									
									//Four on a row
									if(i+3 <= 7 && j-3 >= 0 && board.boardState[j-3][i+3] == turn)
									{
											board.minimaxValue -= 100;
									}
							}
					}
					
					//i-1 & j+1
					//Two in a row
					if(i-1 >= 0 && j+1 <= 6 && board.boardState[j+1][i-1] == turn)
					{
							board.minimaxValue -= 1;
							
							//Three in a row
							if(i-2 >= 0 && j+2 <= 6 && board.boardState[j+2][i-2] == turn)
							{
									board.minimaxValue -= 50;
									
									//Four on a row
									if(i-3 >= 0 && j+3 <= 6 && board.boardState[j+3][i-3] == turn)
									{
											board.minimaxValue -= 100;
									}
							}
					}
					
					//i-1 & j-1
					//Two in a row
					if(i-1 >= 0 && j-1 >= 0 && board.boardState[j-1][i-1] == turn)
					{
							board.minimaxValue -= 1;
							
							//Three in a row
							if(i-2 >= 0 && j-2 >= 0 && board.boardState[j-2][i-2] == turn)
							{
									board.minimaxValue -= 50;
									
									//Four on a row
									if(i-3 >= 0 && j-3 >= 0 && board.boardState[j-3][i-3] == turn)
									{
											board.minimaxValue -= 100;
									}
							}
					}	
				}
			}
		}
	}
	////END DETERMINE MINIMAX VALUE////
	
    ////PRINT BOARD////
	static void printBoard(Board board)
	{
		for(int i = 0; i < 7; i++)
		{
			for(int j = 0; j < 8; j++)
			{
				System.out.print(board.boardState[i][j] + " ");
			}
			System.out.println("");
		}
	}
	////END PRINT BOARD////
	
	////MAKE MOVE////
	static void playerMove(Board board, int turn)
	{
		Scanner input = new Scanner(System.in);
		System.out.println("Choose your move: ");
		int move = input.nextInt();
		
		for(int i = 6; i >=0; i --)
		{
			if(board.boardState[i][move] == 0)
			{
				board.boardState[i][move] = turn;
				return;
			}
		}
	}
	////END MAKE MOVE////
	
	////CHECK GAME STATE////
	static int checkGameState(Board board, int turn)
	{
		for(int i = 0; i < 8; i++)
		{
			for(int j = 6; j >= 0; j--)
			{
				if(board.boardState[j][i] == turn)
				{
					//i-1
					//Two in a row
					if(i-1 >= 0 && board.boardState[j][i-1] == turn)
					{
							
							
							//Three in a row
							if(i-2 >= 0 && board.boardState[j][i-2] == turn)
							{
									
									
									//Four on a row
									if(i-3 >= 0 && board.boardState[j][i-3] == turn)
									{
											return turn;
									}
							}
					}
					
					//i+1
					//Two in a row
					if(i+1 <= 7 && board.boardState[j][i+1] == turn)
					{
						
							
							//Three in a row
							if(i+2 <= 7 && board.boardState[j][i+2] == turn)
							{
								
									
									//Four on a row
									if(i+3 <= 7 && board.boardState[j][i+3] == turn)
									{
											return turn;
									}
							}
					}
					
					//j-1
					//Two in a row
					if(j-1 >= 0 && board.boardState[j-1][i] == turn)
					{
							
							
							//Three in a row
							if(j-2 >= 0 && board.boardState[j-2][i] == turn)
							{
									
									
									//Four on a row
									if(j-3 >= 0 && board.boardState[j-3][i] == turn)
									{
											return turn;
									}
							}
					}
					
					//j+1
					//Two in a row
					if(j+1 <= 6 && board.boardState[j+1][i] == turn)
					{					
						
							
							//Three in a row
							if(j+2 <= 6 && board.boardState[j+2][i] == turn)
							{					
								
									
									//Four on a row
									if(j+3 <= 6 && board.boardState[j+3][i] == turn)
									{					
											return turn;
									}
							}
					}
					
					//i+1 & j+1
					//Two in a row
					if(i+1 <= 7 && j+1 <= 6 && board.boardState[j+1][i+1] == turn)
					{
							
							
							//Three in a row
							if(i+2 <= 7 && j+2 <= 6 && board.boardState[j+2][i+2] == turn)
							{
									
									
									//Four on a row
									if(i+3 <= 7 && j+3 <= 6 && board.boardState[j+3][i+3] == turn)
									{
											return turn;
									}
							}
					}
					
					//i+1 & j-1
					//Two in a row
					if(i+1 <= 7 && j-1 >= 0 && board.boardState[j-1][i+1] == turn)
					{
							
							
							//Three in a row
							if(i+2 <= 7 && j-2 >= 0 && board.boardState[j-2][i+2] == turn)
							{
									
									
									//Four on a row
									if(i+3 <= 7 && j-3 >= 0 && board.boardState[j-3][i+3] == turn)
									{
											return turn;
									}
							}
					}
					
					//i-1 & j+1
					//Two in a row
					if(i-1 >= 0 && j+1 <= 6 && board.boardState[j+1][i-1] == turn)
					{
							
							
							//Three in a row
							if(i-2 >= 0 && j+2 <= 6 && board.boardState[j+2][i-2] == turn)
							{
								
									
									//Four on a row
									if(i-3 >= 0 && j+3 <= 6 && board.boardState[j+3][i-3] == turn)
									{
											return turn;
									}
							}
					}
					
					//i-1 & j-1
					//Two in a row
					if(i-1 >= 0 && j-1 >= 0 && board.boardState[j-1][i-1] == turn)
					{
							
							
							//Three in a row
							if(i-2 >= 0 && j-2 >= 0 && board.boardState[j-2][i-2] == turn)
							{
									
									
									//Four on a row
									if(i-3 >= 0 && j-3 >= 0 && board.boardState[j-3][i-3] == turn)
									{
											return turn;
									}
							}
					}	
				}
			}
		}
		return -1;
	}
	////END CHECK GAME STATE////
}
